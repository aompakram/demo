//Interface
interface User {
    name: String;
    id: number;
}

const user: User = {
    name: 'John',
    id: 0,
}

//Composing type -> union
type WindowStates = "open" | "closed" | "minimized";
type LockStates = "locked" | "unlocked";
type OddNumberUnderTen = 1|3|5|7|9;
const odd : OddNumberUnderTen = 5;
const getLength = (param :string |string[])=>{
    return param.length;
}

getLength('test');