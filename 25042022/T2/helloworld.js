// let a = 5;
// let b = 6;
// let c = a + b;
// console.log(c);
// let num1 : number = 5;
// let first_name : string = "Umaporn";
// let isPresent : boolean = true;
// let myVar : any = "this is my varirable";
//ประกาศ arr แบบที่ 1
// var myArr : string[];
// myArr = ["Aom","2","3","4"];
// console.log(myArr[0]);
// console.log(myArr[1]);
// console.log(myArr[2]);
// console.log(myArr[3]);
//ประกาศ arr แบบที่ 2
// var myArr : number[] = new Array(1,2,3,4);
// var myArr : string[] = new Array("1","2","3","4");
// for (var i = 0 ; i<myArr.length;i++){
//     console.log(myArr[i]);
// }
//การสร้าง func และการเรียกใช้
function calc_discount(price, rate) {
    if (rate === void 0) { rate = 0.5; }
    var discount = price * rate;
    console.log("Discount Amount : " + discount);
}
calc_discount(1000);
calc_discount(1000, 0.3);
